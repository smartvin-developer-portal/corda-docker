# smrtVIN in Docker
## Release Notes
* BMBS, Notary nodes and issue vehicle api
* Issue vechile event with mq
* LSH and ownership transfer api

## 0. Install JDK in Ubuntu
Oracle JDK 8 JVM - minimum supported version 8u131
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

## 1. Install Docker in Ubuntu
**Docker 17.09 and up is required for this Dockerfile.**

Install docker-ce following this reference: https://docs.docker.com/install/linux/docker-ce/ubuntu/

After docker installed,
 `sudo docker -v` should display docker's version

## 2. Git clone this repository to your workspace
```
$ git clone https://gitlab.com/smartvin-developer-portal/corda-docker.git
```

## 3. Bootstrapping network for corda
The nodes see each other using the network map. This is a collection of statically signed node-info files, one for each node in the network. Most production deployments will use a highly available, secure distribution of the network map via HTTP.

In addition to the network map, all the nodes on a network must use the same set of network parameters. These are a set of constants which guarantee interoperability between nodes. The HTTP network map distributes the network parameters which the node downloads automatically

```
$ cd corda-docker
$ java -jar network-bootstrapper-corda-3.1.jar nodes
```
the command will generate nodes' directory for us as below:
```
$ tree -L 2 nodes
nodes
├── BMBS
│   ├── additional-node-infos
│   ├── certificates
│   ├── corda.jar
│   ├── cordapps
│   ├── logs
│   ├── network-parameters
│   ├── node.conf
│   ├── nodeInfo-2224690BD0A834EDE0202AFF4A4EA2624BB043DA4464C5706203901F9E91F766 #generated
│   └── persistence.mv.db
├── LSH
│   ├── additional-node-infos
│   ├── certificates
│   ├── corda.jar
│   ├── cordapps
│   ├── logs
│   ├── network-parameters
│   ├── node.conf
│   ├── nodeInfo-96AADFD07507CB2398D111DCAA350A6874599F1FEAB9207D09F0224D9829839B
│   └── persistence.mv.db
├── Notary
│   ├── additional-node-infos
│   ├── certificates
│   ├── corda.jar
│   ├── cordapps
│   ├── logs
│   ├── network-parameters
│   ├── node.conf
│   ├── nodeInfo-65B670371BB5F63D25527A3C515AABFBE0B1380D11EB9DB82563E53012A232BA
│   └── persistence.mv.db
└── whitelist.txt
```

## 4. Using docker compose
*  Check Dockerfile and docker-compose.yml (e.g. to adjust version or exposed ports)
* `docker-compose build` - to build base Corda images for Corda Node
* `docker-compose up` - to spin up all Corda containers
* `docker exec -it BMBS /bin/sh` - to log in to one of the running Node containers

## Corda docker configuration
At the moment java options are put into **corda_docker.env**. All the others are in Dockerfile/docker_compose.yml

## API
### Issue a vehicle
```
curl -X POST \
  http://localhost:8080/api/smartvin/vehicles \
  -H 'Content-Type: application/json' \
  -d '{
  "VIN": "1HGBH41JXMN109187",
  "issuer": "BMBS",
  "msrp": "",
  "titleDoc": "",
  "gpsLocation": "",
  "manufacturer": "",
  "baumuster": "",
  "body": "",
  "engine": "",
  "steering": "",
  "factory": ""
}'
```

### Get the vehicle by VIN
```
curl -X GET http://localhost:8080/api/smartvin/vehicles/1HGBH41JXMN109187
```

### Get all issued vehicles
```
curl -X GET http://localhost:8080/api/smartvin/vehicles
```

### Transfer ownership of vehicle
```
curl --request POST \
  --url http://localhost:8080/api/smartvin/vehicles/1HGBH41JXMN109187/ownership-transactions \
  --header 'Content-Type: application/json' \
  --data '{"VIN": "1HGBH41JXMN109187", "transferFrom": "BMBS", "transferTo": "LSH", "amount": 100000, "addedValue": 1000}'
```
verify vehicle has been transferred to `LSH`
```
curl -X GET http://localhost:8081/api/smartvin/vehicles
```

## Message queue
After `docker-compose up` is ready, go to your brower and open http://localhost:15672/, the initial user name and password are the same `guest`

Issue vehicle event will be triggered when you are calling the issue vehicle api. After that, you will see the event in `Overview` tab and also it is appeared in `Exchanges` tab as a VEHICLE name. 